ഡെബിയനിലും മറ്റു സ്വതന്ത്ര സോഫ്‌റ്റ്‌വെയറിലും പങ്കെടുക്കുമ്പോൾ/പങ്കെടുക്കാൻ ശ്രമിച്ചപ്പോൾ ഉണ്ടായ അനുഭവങ്ങൾ പങ്കുവെക്കൽ


പങ്കെടുക്കുന്നവരുടെ പേരുകൾ കാണുന്നില്ല. അതുകൊണ്ട് ആരാണുസംസാരിക്കുന്നത് എന്നുമനസ്സിലാകുന്നില്ല - ജിഷ്ണു 
- Syam G (moderator)
- Abhijith (bhe)
- Subin Siby
- Ranjith Siji
- Ambady Anand S
- Akhil
- Kiran (talkmeister)

- Student Programs
  * Outreachy
  * Google Summer of Code
  * Google Season of Docs
  * FOSSUnited: https://fossunited.org/
Nandaja won Linux Foundation Scholarship - https://www.linuxfoundation.org/blog/2013/09/linux-training-scholarship-winners-represent-new-generation-of-talent/

Q.  Do you think that the inertia towards official adoption of more modern platforms for code hosting and communication is preventing newer generation from joining these FOSS projects? - Abraham Raji (avronr) :)
Q.  It isn't about moving towards a platform but trying to improve current systems to give better UX as technology moves foreward. Is enough effort being put into that area? This is a sub question to previous question.


Q. In a world where containers are getting popular, do you think debian as a desktop operating system may loose it's charm? -
It depends on how Debian community will manage to get more people to contribute to keep up with the change in attitude. It is becoming harder, no doubt. Another option may be Debian becoming just a base system for containers - Praveen


