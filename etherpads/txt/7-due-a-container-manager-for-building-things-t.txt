== DUE  debconf presentation notes / remarks ==

Next is text that was written without knowing the content of the Debconf20 presentatin

Q: When will it be in Debian?
A:  Work in Progress, details at https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=961371
And in Geert's defense - I had a critical project at work that kept me from following up on his feedback for a few weeks at a time.  -Alex

Q: Can I already do `debcheckout` or `dgit checkout`?
A: `debcheckout  --git-track debian-upstream https://github.com/ehdoyle/DUE due`
Note that with this fork ^^ I'm reordering things to follow the model of upstream tarball + Debian packaging.  There's a slightly older version that will build an installable deb here - but that will be replaced with the contents of the first repository once everything is up to standards ;)
https://github.com/CumulusNetworks/DUE
A:    ....... here your  dgit  knowledge ......

Q: What to do after `debcheckout`?
Answer
* cd due
* make orig.tar
* git checkout debian-upstream
* echo '3.0 (quilt)' > debian/source/format
* debuild -uc -us
* #  two lintian warnings
* sudo dpkg --install ../due_*.deb
* due --version
* due --help


Q: Have you those commands  copy and paste ready?
debcheckout  --git-track debian-upstream https://github.com/ehdoyle/DUE due
cd due
make orig.tar
git checkout debian-upstream
echo '3.0 (quilt)' > debian/source/format
debuild -uc -us
#  two lintian warnings
sudo dpkg --install ../due_*.deb
due --version
due --help

Q: does the DUE build tool  also work with https://tracker.debian.org/pkg/podman instead of docker.io and how does it relate to https://tracker.debian.org/pkg/buildah
Good question - right now it's very docker specific, so I would think that it wouldn't intersect there, but I'll take a look at both projects to get you a better answer.
-- let's touch base after the talk, I've packaged all of those packages and maintain them in debian, and have written a blogpost that may do something similar in spirit to what you're presenting at http://tauware.blogspot.com/2020/04/building-packages-with-buildah-in-debian.html

Have you ever done a comparison with sbuild? (I know they are different approaches, but maybe a comparison is good?)
So it's kind of different scopes. While DUE focuses on the overall build evironment, it does have some package build support in the form of a defualt build script every container has ( Think of it as an abstraction so that the invoking DUE application can tell a container to 'just build' and have some hope of success ). However that's not nearly as extensive as what sbuild does.
(I believe somwhere in there I call it a 'low rent sbuild' :) )


Can you build for a different architecture with DUE?
Yes - if you use a base container of antoher architecture, DUE will use qemu to run emulation - so it's not a cross compile, but a build in an emulated environment.  I've done this with ARM.

<Programmin (IRC #dc20-talks)> Would DUE help build a Fedora/Click/other type package if I have a build for building Python app .deb?
At the moment, it's got depenencies around package install that are very specifically Debian, however I think those could be abstracted out  on a per OS basis, so apart from that it should be possible to do the same sorts of things for Fedora. It's just a question of work and time :)

<wookey_ (IRC #dc20-talks)> Is it me or does DUE just implement all the things schroot does (but with containers instead of chroots)?
Yes - it's a similar funcitonal overlap. We found that it was easier  to manage Docker containers on multi user systems at Cumulus, but it is very similar in terms of the fuctionality you'd get out of one or the other.
(And given how well schroots have been working, I don't nearly see DUE as a replacement, but more of an alternative where one can whip something up quickly and get an environemnt that can be easily run on different systems)

