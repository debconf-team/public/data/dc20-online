Welcome to the LTS team BoF!


Who's here? Please mention your nicks.

1. utkarsh2102
2. h01ger
3. bittin
4. el_cubano
5. lamby
6. abhijith
7. bwh
8. suntong
9. Beuc
10. noahm
11. larjona
12. federico3
13. lisandro
14. raphael
15. pochu
16. foka (late)
17. capote (late)
18. Delib
19. vicm3


8<---8<---8<---8<---8<---8<---8<---8<---8<---8<---8<---8<---

Agenda:

0. Introduction
	https://wiki.debian.org/LTS
	stretch LTS since July 18th 2020
	15 regular contributors
	Talk to h01ger after the talk if you want to join

1. Presentation of preliminary results from the recent LTS survey Q: Will the survey results be posted to the ML or such so you can read it yourself too? -- Holger said yes :) 
2. Q & A

Thanks a lot for your great work Debian LTS Team! +1+1+1+1+1

Q. I just wanted to note that the fact that it is used in big organization does not necessarily means it is heavily used in a big organization. It might be a small setup and thus the organization itself might not even notice it. +1+1 - also some of those "big organizations" are probably public administrations where it's really very difficult to make them sponsor "anything".  ← BIG +1

Comment. (academic nitpick, feel free to ignore) Very interesting survey answers! I only feel the need to note that, as a possible "threat to the validity" of the study, it asked Debian-friendly people - So there is a strong positive bias for anything improving Debian. Of course, if you acknowledge that bias... you are "vaccinated" against that threat ;-) (meh, academic-orientedness...) It *could* be interesting/enlightening to do a similar survey on wider-scale Linux forums..? 

Q . Are there any other statistical analyses availiable, like comparision between groups (correlation, variance analysis, at least crosstabs)?
not yet, this is the first time we publish bits of the results. a written publication is planned.
Ok; I hope the raw data are/will be published as well.

Comment. The Debian cloud team is continuing to publish stretch LTS images for AWS, Microsoft Azure, and OpenStack. Google is apparently going to continue publishing stretch for GCE as well. stretch remains quite popular in cloud environments.

From IRC:
    
    15:21 <matthew_hen> can someone ask on the pad how can we help?
    
   A: Review and test packages before we publish them, subscribe to the mailinglist and we will ask for help with package testing when we need it 
    We are working like the Security team and use their tracker -- but not their mailing list. I'd love to see LTS security updates announced on debian-security-announce. Would help reduce confusion among users who don't really know or care about the distinction between stable and LTS.
    Getting awareness that the Debian LTS team exists
    Adding autopkgtests to packages that have security updates
    Follow https://salsa.debian.org/lts-team/lts-extra-tasks/-/issues
    
    
    Q: What are the challenges that will come up for LTS in the future?
    A: Packages that will deprecated (for example, Python 2.x)
    Getting more people to contribute as the archives are growing
    Possibly see how to better co-operate with CIP (cip-project.org) Civil Infrastructure Project, they are sponsors of LTS/ELTS already (Raphael Hertzog)
    Find ways to contribute back to other parts of Debian that are impacted by LTS, for example, https://salsa.debian.org/freexian-team/project-funding
    
Q: Why is LTS not officially part of Debian? 
→ I understand part of the question as to, why it is independently funded. And I'd venture the answer - Debian does not pay you, LTS contributors, for your work; LTS (its sponsors via...) does. (said as a non-LTS-person in Debian)
It is an official part of Debian.
taking from wiki: Debian LTS is not handled by the Debian security team, but by a separate group of volunteers and companies interested in making it a success 
(Many developers have paid jobs sponsoring their work on individual projects in Debian. That does not make those projects "not Debian".  Debian does not pay for projects, in general.)

Q: On the survey result presentation - I'm surprised there were no critical comments from the free-form comments presented.
A: There was one, it was also a big and obscene rant so not really worth relaying :-)
Honest question: Have some replies not been processed yet or got lost? I'm aware of some constructive critical ideas.
A: Oh, I didn't read them all - that was the one that was most obvious
A: None of the responses are lost. We tried to cherry-pick the most interesting ones. It'll be hopefully better visible in the results when they'll be shared or something. Alright, thanks.


Q: In etherpad chat > Do people need to know about LTS, or can they just keep using oldstable and continue getting updates, etc? (uninformed bystander) they need to be aware since not every package is supported in LTS, but there is https://packages.debian.org/buster/debian-security-support



Q: Are we having a monthly meeting tomorrow over Jitsi or IRC?
A: Lets discuss this on IRC after the BoF and decide then and there

Q: what packages are maintained as part of LTS? I assume that backporting security fixes for the entire archive is both too work-intensive and also not beneficial
A: see src:debian-security-support :) https://salsa.debian.org/debian/debian-security-support/-/blob/master/security-support-ended.deb9 We don't support Xen now for LTS Stretch -- We used to work with Credativ with work to help with support Xen but to speculative CPU security holes we stopped doing so as the Security Team decided that

Q: Make dot releases in co operation with FTP Master is this something we have tried?
A: We have not tried very hard, not sure whether we want it or not, its always nice to have images/cloud images with all security updates applied to have lighter updates to run on first boot, but it's not only the FTP Masters work, if we want images we have to work with the Debian CD, installer and Cloud team aswell as it would require work on them as well if we don't get involved in all of those teams ourselfs and as no one has whined that this is missing we have not made any huge efforts in these directions




