Debian Med BoF
===============

Slides are online at
    https://people.debian.org/~tille/talks/20200823_debconf_debian-med_bof/

<utkarsh2102> Is there someone who we can contact for mentorship?
Yes! https://wiki.debian.org/DebianMed/MoM

<yuvrajm (IRC)> Is GNU Health https://www.gnuhealth.org/#/ anyway connected to Debian Med?

<maddoghall>  I am glad you listened to me at DebConf 15

<bittin> BigBlueButton only supports some older Ubuntu version atm :: https://github.com/bigbluebutton/bigbluebutton/issues/8861

List of packages accepted by the Debian FTP team as part of the COVID-19 Debian Med hackathons
https://salsa.debian.org/med-team/community/2020-covid19-hackathon/-/wikis/NEW-Requests-for-Covid-19-Research-Infrastructure#done-by-ftp-team-thanks-a-lot-for-the-support-during-and-after-first-sprint-do-not-edit

<utkarsh2102> Can you also post the work/packages that need work and loving? :)
Yes! https://salsa.debian.org/med-team/community/2020-covid19-hackathon/-/wikis/COVID-19-Hackathon-packages-needing-work

<utkarsh2102> Debian Packaging FAQ(s): https://salsa.debian.org/med-team/community/2020-covid19-hackathon/-/wikis/Debian-Packaging-FAQ



<crusoe> Thanks for the great questions!)
how can i join the online video? -- https://jitsi.online.debconf.org/22-debian-med-bof-WuqHpaDMrrmQCADG

The SIMD Everywhere library: https://wiki.debian.org/SIMDEverywhere


