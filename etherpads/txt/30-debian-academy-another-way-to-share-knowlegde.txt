Debian Academy: Another way to share knowledge about Debian

    
/*
History: there were tutorials on IRC, at #debian-meeting, organized by Debian Women
https://wiki.debian.org/DebianWomen/Projects/Events/TrainingSessions
*/
A: Thanks for pointing this.

Q:  Is Valeria a real person? (-:
A: Let's see :) No, she is not :D

Additionally: webinars on (jitsi or) bigbluebutton
A: Yes, those kind of contents and tools could work too.

Q. Do you have a set of topics in mind?
A: Initially I have thought about Debian Packaging, Debian tools used for communication in the community (IRC, Mailing Lists) translations (l10n), Reproducible Builds and Debian Salsa, just my ideas, but it should be a project for every topic as possible from the technical to the social thing.


Comment: there's some videos about package management (not packaging) at https://highvoltage.tv/videos/watch/playlist/7bac4666-6ab7-4acb-92ee-70dac2e4a014
A: Thanks. Yes, even some of these videos were kind of inspiration to think about the Debian Academy project.

Q:  Jathan, What do you believe are first steps to creating such an acadamy?  What hurdles to jump?   Ideas of how to organize trainers?  What role would you like to have? +1
A:  Use inspiration from tech used by Debian Social (federated web etc) but focused on education with a centralized Debian Acadamy platform.
A:  Jathan would like a social communications role for sharing and building awareness of this idea.
A: I think the first steps should be get together all the people who find this purpose relevant, create a team named "Debian Academy", to create a Mailing List, IRC channel, to organise the brainstorming and start the planning. The hurdles to jump currently are just to start :) To have ideas about how to organise the trainers, first we need to have people who desire to be trainers and then see what to define. Regarding to the role, I would like to help with the infrastructure of Debian Academy and with the communication.

Q: Would you put your email up again?
jathan@d.o

COMMENT: Eriberto (eriberto@d.o) has an entire structured Debian packaging course on http://debianet.com.br/, with SEVERAL hours of video + a , and has been using this for several years to train a lot of people in packaging. I think his experience is something that this could build upon. (pt_BR though)
A: Of course and he is a wonderful teacher! I met him at DC19 in Curitiba and I agree about his training contents, a complete course but currently available only in Portuguese and hard to find since it is on his personal web site.

COMMENT: e-learning (or a debian academy) needs more than a series of video tutorials, first a concept, a "curriculum", didactic background, target groups …
A:  I agree with this point (Jathan says).  Lots of skills needed here, and need team members to decide.

Q: Are this platform going to be for end-users or just for more advanced users?
A: Any type of level.

COMMENT: If you show prerequsites of knowledge needed to understand the topic, then you could start anywhere up and down the curriculum.

COMMENT: You could build the course content around the objectives of a certification such as LPI's LPIC-1 or Linux Essentials.   Then people following your courses could get a certificate.   That gives you structure and flow that is useful for (perhaps) getting a job.
A: That could be something useful related to Debian and jobs. Certifications are not my main goal for Debian Academy, but if it could be interesting to see what happen if Debian Academy give diplomas or certifications to people interested on them.

Q. You say videos are not structured. Would you consider that making a browseable index of existing material (DebConf or external) categorized by topic and language be an acceptable starting point? +1 +1, curating existing material could be *great*, +1 peertube makes the titles searchable at least
A:  Yes a great way to start.

Comment: seems there is some indexing at https://salsa.debian.org/debconf-video-team/archive-meta pretty minimal, it's just data about the structure, not much on the content (maybe a place to improve?) -- Metadata is the most difficult part. I [alvarezp] tried "digitalizing" metadata in a spreadsheet before finding out we already had archive-meta so it's nice to see that the most difficult part is already done.
A: Thanks for sharing that metadata repo!

Q: Many of the materials that would be created for this academy you propose would be general, about Linux - And there is no shortage of those! Defining a project scope is fundamental for its success. So, where would you draw the line? How "deep" into free software use and development would you call adequate for the Debian Academy, and how much would you leave fo others? 
A: Absolutely. The scope will be focused only to Debian and specifics contents around its different fields, not in Free Software and GNU/Linux in general. I would draw the line based on the current activities and existing teams of the Debian Project.


COMMENT:  One way to determine scope of trainings is to discover motivations of a first set of contributors, but contributors need to know scope to decide if they should attempt contributions. . . So a chicken and egg problem.
A: Not really a chicken and egg problem. The scopes should be focused initially on the main topics that are hard to understand for someone new interested about helping directly to Debian.
