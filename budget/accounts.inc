account assets:cash
account assets:debian-ch
account assets:debian-france
account assets:stripe
account assets:SPI
account assets:dc21:SPI
account assets:dc22:SPI
account assets:dc23:SPI
account assets:dc21:debian-france
account assets:dc22:debian-france

account expenses:bursaries:expense
account expenses:fees
account expenses:general
account expenses:incidentals
account expenses:swag:t-shirt
account expenses:swag:shipping
account expenses:tax
account expenses:video
account expenses:voip
account income:donations
account income:registration
account income:registration:stripe for debian-france
account income:registration:SEPA for debian-france
account income:sponsors:bronze
account income:sponsors:gold
account income:sponsors:platinum
account income:sponsors:silver
account income:sponsors:supporter
account income:sponsors:donations
account income:future-sponsors:dc21
account income:future-sponsors:dc22
account income:future-sponsors:dc23

account liabilities:dlange
account liabilities:freewear
account liabilities:indiebio
account liabilities:lenharo
account liabilities:nattie
account liabilities:srud
