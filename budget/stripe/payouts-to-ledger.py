#!/usr/bin/env python3

from csv import DictReader

payouts = []
with open('payouts.csv') as f:
    r = DictReader(f)
    for row in r:
        payouts.append(row)

payouts.sort(key=lambda p: p['Created (UTC)'])

for payout in payouts:
    created = payout['Created (UTC)']
    date = created.split()[0]
    amount = payout['Amount'].replace(',', '.')
    currency = payout['Currency'].upper()
    print(f'{date} * Stripe Payout')
    print(f'    assets:debian-france    {amount} {currency}')
    print('    assets:stripe')
    print()
