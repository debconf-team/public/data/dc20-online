#!/usr/bin/env python3

from csv import DictReader

payments = []
with open('payments.csv') as f:
    r = DictReader(f)
    for row in r:
        payments.append(row)

payments.sort(key=lambda p: p['Created (UTC)'])

for payout in payments:
    created = payout['Created (UTC)']
    date = created.split()[0]

    amount = payout['Amount'].replace(',', '.')
    converted_amount = payout['Converted Amount'].replace(',', '.')
    fee = payout['Fee'].replace(',', '.')

    currency = payout['Currency'].upper()
    converted_currency = payout['Converted Currency'].upper()
    description = payout['Description']

    print(f'{date} * {description}')
    print(f'    ; for {amount} {currency}')
    print(f'    income:registration    -{converted_amount:8} {converted_currency}')
    print(f'    expenses:fees           {fee:8} {converted_currency}')
    print('    assets:stripe')
    print()
